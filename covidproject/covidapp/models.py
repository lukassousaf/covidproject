from django.db import models


# Create your models here.


class Doenca(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=50)
    sintomas = models.CharField(max_length=150)
    prevencao = models.CharField(max_length=150)


class Diagnostico(models.Model):
    id = models.AutoField(primary_key=True)
    doenca = models.ForeignKey(
        Doenca,
        on_delete=models.DO_NOTHING,
        unique=False
    )
    dataDiagnostico = models.DateField(auto_now=True, null=False)


class Paciente(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=50)
    tipoSanguineo_CHOICES = (
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('AB', 'AB'),
        ('O+', 'O+'),
        ('O-', 'O-')
    )

    tipoSanguineo = models.CharField(max_length=2, choices=tipoSanguineo_CHOICES, verbose_name="Tipo Sanguíneo")
    cpf = models.CharField(max_length=11, verbose_name="CPF")
    dataNascimento = models.DateField(null=False, verbose_name="Data de Nascimento")
    SEXO_CHOICES = (
        ('M', 'Masculino'),
        ('F', 'Feminino')
    )
    sexo = models.CharField(max_length=1, choices=SEXO_CHOICES)
    sintomas = models.CharField(max_length=150)
    dataCadastramento = models.DateField(auto_now=True, null=False)
    gravidade_CHOICES = (
        ('Leve', 'Leve'),
        ('Medio', 'Media'),
        ('Grave', 'Grave'),
    )
    gravidade = models.CharField(max_length=5, choices=gravidade_CHOICES)
    antecedentesPatologicos = models.CharField(max_length=70, verbose_name="Antecedentes Patológicos")
    descricao = models.CharField(max_length=70, verbose_name="Descrição")
    diagnostico = models.OneToOneField(
        Diagnostico,
        on_delete=models.CASCADE,
        null=True, verbose_name="Diagnóstico"
    )

