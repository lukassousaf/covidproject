from django.shortcuts import render, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from .models import *
from django.forms import ModelForm
from django.views.decorators.csrf import csrf_protect
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Pacientea

class PacienteForm(ModelForm):
    class Meta:
        model = Paciente
        fields = ['nome', 'tipoSanguineo', 'cpf', 'dataNascimento', 'sexo', 'sintomas', 'gravidade', 'antecedentesPatologicos', 'descricao']


def paciente_list(request, template_name='paciente_list.html'):
    pacientes = Paciente.objects.all()
    for paciente in pacientes:
        if paciente.diagnostico is None:
            diagnosticar(paciente.sintomas, paciente.id)
    pacientes = {'lista': pacientes}
    return render(request, template_name, pacientes)


def paciente_new(request, template_name='paciente_form.html'):
    form = PacienteForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect('listar_pacientes')
    return render(request, template_name, {'form': form})

def paciente_edit(request, pk, template_name='paciente_form.html'):
    paciente = get_object_or_404(Paciente, pk=pk)
    if request.method == "POST":
        form = PacienteForm(request.POST, instance=paciente)
        if form.is_valid():
            paciente = form.save()
            return redirect('listar_pacientes')
    else:
        form = PacienteForm(instance=paciente)
    return render(request, template_name, {'form': form})


def paciente_remove(request, pk):
    paciente = Paciente.objects.get(pk=pk)
    if request.method == "POST":
        paciente.delete()
        return redirect('listar_pacientes')
    return render(request, 'paciente_delete.html', {'paciente': paciente})

# Doenças

class DoencaForm(ModelForm):
    class Meta:
        model = Doenca
        fields = ['nome', 'sintomas', 'prevencao']

def doenca_list(request, template_name='doenca_list.html'):
    doenca = Doenca.objects.all()
    doencas = {'lista': doenca}
    return render(request, template_name, doencas)


def doenca_new(request, template_name='doenca_form.html'):
    form = DoencaForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('listar_doencas')
    return render(request, template_name, {'form': form})


def doenca_edit(request, pk, template_name='doenca_form.html'):
    doenca = get_object_or_404(Doenca, pk=pk)
    if request.method == "POST":
        form = DoencaForm(request.POST, instance=doenca)
        if form.is_valid():
            doenca = form.save()
            return redirect('listar_doencas')
    else:
        form = DoencaForm(instance=doenca)
    return render(request, template_name, {'form': form})


def doenca_remove(request, pk):
    doenca = Doenca.objects.get(pk=pk)
    if request.method == "POST":
        doenca.delete()
        return redirect('listar_doencas')
    return render(request, 'doenca_delete.html', {'doenca': doenca})


def diagnosticar(sintomas, primary_key):
    try:
        paciente = Paciente.objects.get(pk=primary_key)
        doenca = Doenca.objects.filter(sintomas=sintomas)

        if len(doenca) > 0:
            diagnostico = Diagnostico()
            diagnostico.doenca = doenca[0]
            paciente.diagnostico = diagnostico
            diagnostico.save()
            paciente.save()
        return redirect('listar_pacientes')
    except NameError:
        return Exception("Doença não encontrada.")


def logout_user(request):
    logout(request)
    return redirect('/login/')


@login_required(login_url='/login/')
def index(request):
    return render(request, 'paciente_list.html')


def login_user(request):
    return render(request, 'login/login.html')


@csrf_protect
def submit_login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('listar_pacientes')
        else:
            messages.error(request, 'Usuário ou senha inválida, tente novamente!')
    return redirect('/login/')


