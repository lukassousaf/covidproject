from django.urls import path
from django.conf.urls import url
from .views import *
from django.contrib.auth.views import LoginView
from . import views

# app_name = "users"

urlpatterns = [
    url(r'^paciente_list/', paciente_list, name='listar_pacientes'),
    url(r'^paciente_new/', paciente_new, name='paciente_new'),
    url(r'^paciente_edit/(?P<pk>[0-9]+)', paciente_edit, name='paciente_edit'),
    url(r'^paciente_remove/(?P<pk>[0-9]+)', paciente_remove, name='paciente_remove'),

    url(r'^doenca_list/', doenca_list, name='listar_doencas'),
    url(r'^doenca_new/', doenca_new, name='doenca_new'),
    url(r'^doenca_edit/(?P<pk>[0-9]+)', doenca_edit, name='doenca_edit'),
    url(r'^doenca_remove/(?P<pk>[0-9]+)', doenca_remove, name='doenca_remove'),
    # path('login/', LoginView.as_view(template_name='users/login.html'), name="login")
    path('login/', views.login_user),
    path('login/submit', views.submit_login),
    path('logout/', views.logout_user, name="logout"),
    path('', views.index),
]
